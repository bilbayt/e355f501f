using Xunit;

namespace AdjacentElementsProduct;

public class Tests
{
	[Fact]
	public void Test1()
	{
		var input = new[] { 3, 6, -2, -5, 7, 3 };

		var actual = Program.GetAdjacentElementsProduct(input);

		var expected = 21;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test2()
	{
		var input = new[] { -1, -2 };

		var actual = Program.GetAdjacentElementsProduct(input);

		var expected = 2;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test3()
	{
		var input = new[] { 5, 1, 2, 3, 1, 4 };

		var actual = Program.GetAdjacentElementsProduct(input);

		var expected = 6;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test4()
	{
		var input = new[] { 1, 2, 3, 0 };

		var actual = Program.GetAdjacentElementsProduct(input);

		var expected = 6;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test5()
	{
		var input = new[] { 9, 5, 10, 2, 24, -1, -48 };

		var actual = Program.GetAdjacentElementsProduct(input);

		var expected = 50;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test6()
	{
		var input = new[] { 5, 6, -4, 2, 3, 2, -23 };

		var actual = Program.GetAdjacentElementsProduct(input);

		var expected = 30;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test7()
	{
		var input = new[] { 4, 1, 2, 3, 1, 5 };

		var actual = Program.GetAdjacentElementsProduct(input);

		var expected = 6;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test8()
	{
		var input = new[] { -23, 4, -3, 8, -12 };

		var actual = Program.GetAdjacentElementsProduct(input);

		var expected = -12;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test9()
	{
		var input = new[] { 1, 0, 1, 0, 1000 };

		var actual = Program.GetAdjacentElementsProduct(input);

		var expected = 0;

		Assert.Equal(expected, actual);
	}
}
