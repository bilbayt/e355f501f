using Xunit;

namespace MakeArrayConsecutive;

public class Tests
{
	[Fact]
	public void Test1()
	{
		var input = new[] { 6, 2, 3, 8 };

		var actual = Program.MakeArrayConsecutive(input);

		var expected = 3;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test2()
	{
		var input = new[] { 0, 3 };

		var actual = Program.MakeArrayConsecutive(input);

		var expected = 2;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test3()
	{
		var input = new[] { 5, 4, 6 };

		var actual = Program.MakeArrayConsecutive(input);

		var expected = 0;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test4()
	{
		var input = new[] { 6, 3 };

		var actual = Program.MakeArrayConsecutive(input);

		var expected = 2;

		Assert.Equal(expected, actual);
	}
}
