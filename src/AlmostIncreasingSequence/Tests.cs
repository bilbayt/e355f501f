using Xunit;

namespace AlmostIncreasingSequence;

public class Tests
{
	[Fact]
	public void Test1()
	{
		var input = new[] { 1, 3, 2, 1 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = false;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test2()
	{
		var input = new[] { 1, 3, 2 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = true;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test3()
	{
		var input = new[] { 1, 2, 1, 2 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = false;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test4()
	{
		var input = new[] { 1, 4, 10, 4, 2 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = false;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test5()
	{
		var input = new[] { 10, 1, 2, 3, 4, 5 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = true;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test6()
	{
		var input = new[] { 1, 1, 1, 2, 3 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = false;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test7()
	{
		var input = new[] { 0, -2, 5, 6 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = true;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test8()
	{
		var input = new[] { 1, 2, 3, 4, 5, 3, 5, 6 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = false;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test9()
	{
		var input = new[] { 40, 50, 60, 10, 20, 30 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = false;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test10()
	{
		var input = new[] { 1, 1 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = true;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test11()
	{
		var input = new[] { 1, 2, 5, 3, 5 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = true;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test12()
	{
		var input = new[] { 1, 2, 5, 5, 5 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = false;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test13()
	{
		var input = new[] { 10, 1, 2, 3, 4, 5, 6, 1 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = false;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test14()
	{
		var input = new[] { 1, 2, 3, 4, 3, 6 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = true;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test15()
	{
		var input = new[] { 1, 2, 3, 4, 99, 5, 6 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = true;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test16()
	{
		var input = new[] { 123, -17, -5, 1, 2, 3, 12, 43, 45 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = true;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test17()
	{
		var input = new[] { 3, 5, 67, 98, 3 };

		var actual = Program.GetAlmostIncreasingSequence(input);

		var expected = true;

		Assert.Equal(expected, actual);
	}
}
